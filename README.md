# SFML-Pong
## Programmed by JTKreates

## About

This is a simple Pong game programmed in C++ using SFML. 
Currently it's two player and uses UP and DOWN, and W and S for moving the paddles. 
The ball might be a bit glitchy at times when it hits a paddle weird but overall there don't appear to be any other bugs.

## Compiling

Make sure you have SFML 2.4.2 installed on your system

Just run the following commands to build from the command line.

```g++ -c Pong.cpp```

```g++ -o Pong Pong.o -lsfml-graphics -lsfml-window -lsfml-system```

Notice: You can use clang++ instead of g++ in this occasion

You can also build it by starting a new SFML project in your favorite IDE and then importing the files.


