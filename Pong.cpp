#include <SFML/Graphics.hpp>

int main()
{
    sf::Vector2f pad1P;
    sf::Vector2f pad2P;
    pad1P.x = 0;
    pad2P.x = 790;

    sf::Vector2f ballP;

    float speed = 500;

    sf::RenderWindow window(sf::VideoMode(800, 500), "Pong");

    sf::Texture paddleT;
    paddleT.loadFromFile("res/paddle.png");

    sf::Texture ballT;
    ballT.loadFromFile("res/ball.png");

    sf::Sprite pad1;
    sf::Sprite pad2;
    sf::Sprite ball;
    pad1.setTexture(paddleT);
    pad2.setTexture(paddleT);
    ball.setTexture(ballT);

    ballP.x = 400;
    ballP.y = 1;

    float xV = .2f;
    float yV = .2f;

    bool pad1Up = false;
    bool pad1Down = false;
    bool pad2Up = false;
    bool pad2Down = false;

    sf::Clock clock;

    while(window.isOpen())
    {
        sf::Time dt = clock.restart();
        float dtAsSeconds = dt.asSeconds();
        sf::Event event;

        while(window.pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
                window.close();
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) pad1Up = true;
        else pad1Up = false;

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) pad1Down = true;
        else pad1Down = false;

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) pad2Up = true;
        else pad2Up = false;

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) pad2Down = true;
        else pad2Down = false;

        if(pad1P.y >= 0 && pad1P.y <= 450)
        {
            if(pad1Up == true) pad1P.y -= speed * dtAsSeconds;
            if(pad1Down == true) pad1P.y += speed * dtAsSeconds;
        }
        else if(pad1P.y < 0) pad1P.y += 1;
        else if(pad1P.y > 450) pad1P.y -= 1;

        if(pad2P.y >= 0 && pad2P.y <= 450)
        {
            if(pad2Up == true) pad2P.y -= speed * dtAsSeconds;
            if(pad2Down == true) pad2P.y += speed * dtAsSeconds;
        }
        else if(pad2P.y < 0) pad2P.y += 1;
        else if(pad2P.y > 450) pad2P.y -= 1;

        if(ball.getGlobalBounds().left < 0 || ball.getGlobalBounds().left > 792)
        {
            ballP.y = 1;
            ballP.x = 400;
        }

        if(ball.getGlobalBounds().top < 0 || ball.getGlobalBounds().top > 492)
        {
            yV = -yV;
        }

        if(ball.getGlobalBounds().intersects(pad1.getGlobalBounds()))
        {
            ballP.y -= (xV * 30);
            xV = -xV;
        }

        if(ball.getGlobalBounds().intersects(pad2.getGlobalBounds()))
        {
            ballP.y -= (xV * 30);
            xV = -xV;
        }

        ballP.x += xV;
        ballP.y += yV;

        pad1.setPosition(pad1P);
        pad2.setPosition(pad2P);
        ball.setPosition(ballP);

        window.clear(sf::Color::White);
        window.draw(pad1);
        window.draw(pad2);
        window.draw(ball);
        window.display();
    }
}